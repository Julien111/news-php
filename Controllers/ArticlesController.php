<?php

namespace App\Controllers;

use App\Models\UsersModel;
use App\Models\ArticlesModel;
use App\Models\CategoryModel;

class ArticlesController extends Controller 
{
    /**
     * Cette méthode affichera toutes les annonces de la base de données
     *
     * @return void
     */
    public function index()
    {
        $articlesModel = new ArticlesModel;

        //on va chercher toutes les articles de la BDD

        $articles = $articlesModel->findAll();

         //on génère la vue

        $this->render('articles/index', compact('articles'));
    }

    /**
     * Affiche 1 article selon l'id
     *
     * @param int $id id de l'annonce
     * @return void
     */
    public function read (int $id) 
    {
        //On instancie le model article, category et user
        $articlesModel = new ArticlesModel();
        $users = new UsersModel;
        $categories = new CategoryModel;

        // on va chercher un article et auteur de celui-ci
        
        $article = $articlesModel->find($id);

        //Je change le format date 
        
        $dateA = strtotime($article->created_at);
        $formatDate = date("d-m-Y", $dateA);

        //var_dump($formatDate);
        $category = $categories->find(intval($article->category_id));
        $author = $users->findAUser(intval($article->users_id));

        $this->render('articles/read', compact('article', 'author', 'formatDate', 'category'));
    }
}