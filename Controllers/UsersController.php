<?php

namespace App\Controllers;

use App\Core\Form;
use App\Models\UsersModel;


class UsersController extends Controller
{
    /**
     * Inscription des utilisateurs
     *
     * @return void
     */
    public function register () 
    {
        //on vérifie si le formulaire est valide  

        if(!empty($_POST)):            

            $user = new UsersModel;
            
            if($user->findTheEmail($_POST['email'])):  
                $_SESSION['erreur'] = 'Cet email est déjà utilisé par un utilisateur.';
                header('Location: /');
            endif;

            if(Form::validateRegister($_POST, ['nom', 'email', 'password', 'pass2'])):
                //Le formulaire est valide  

                //On nettoie l'email
                $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);

                // le nom 

                $nom = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);

                //On crypte le password 
                
                $pass = password_hash($_POST['password'], PASSWORD_ARGON2I);

                //On crée une instance de UsersModel et on hydrate un objet
                
                $user->setNom($nom)->setEmail($email)->setPassword($pass)->setRoles('["ROLE_USER"]');

                //On stocke l'utilisateur 

                $user->create();

                //On confirme l'inscription de l'utilisateur

                $_SESSION['message'] = "Votre Inscription est confirmé, connectez-vous pour aller sur votre page profil.";
                header('Location: /');
                exit;
            else:            
                $_SESSION['erreur'] = 'Erreur, les champs ne sont pas remplis correctement ou le mot de passe fait moins de 5 caractères';
                header('Location: /users/register');
                exit;
            endif;
            
        else: 
            echo '';
        endif;

        //creer le formulaire d'inscription 

        $form = new Form;

        $form->debutForm()
            ->ajoutLabelFor('nom', 'Votre nom :', ['class' => 'form-label'])
            ->ajoutInput('text', 'nom', ['id' => 'nom', 'class' => 'form-control', 'placeholder' => 'Nom']) 
            ->ajoutLabelFor('email', 'E-mail : ', ['class' => 'form-label'])
            ->ajoutInput('email', 'email', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'E-mail'])
            ->ajoutLabelFor('password', 'Mot de passe :', ['class' => 'form-label'])
            ->ajoutInput('password', 'password', ['id' => 'pass', 'class' => 'form-control'])
            ->ajoutLabelFor('pass2', 'Confirmez le mot de passe : ', ['class' => 'form-label'])
            ->ajoutInput('password', 'pass2', ['id' => 'pass2', 'class' => 'form-control'])
            ->ajoutBouton('Inscription', ['class' => 'btn btn-primary mt-2'])
            ->finForm();

            $this->render('users/register', ['registerForm' => $form->createForm()]);
    }
}