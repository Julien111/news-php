<?php

namespace App\Controllers;

abstract class Controller
{
    protected $template = 'default';

    public function render(string $fichier, array $datas = [], string $template = 'default')
    {
        //On extrait le contenu des données 
        extract($datas);

        //On démarre le buffer de sortie 

        ob_start();

        //A partir de ce point toute sortie est de conservée en mémoire

        //on extrait le tab et on le transmet à la vue 

        require_once ROOT.'/Views/'.$fichier.'.php';

        //dans le ob_get_clean on transfère le buffer dans contenu
        //le contenu est mis en mémoire 
        //et ensuite il le stock à l'intérieur de contenu

        $contenu = ob_get_clean();

        //on redirige vers le template de la page

        require_once ROOT.'/Views/'.$template.'.php';
    }
}