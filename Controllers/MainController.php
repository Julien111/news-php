<?php

namespace App\Controllers;

use App\Models\ArticlesModel;

class MainController extends Controller 
{
    public function index ()
    {

        $articlesModel = new ArticlesModel;

        $articles = $articlesModel->lastArticles();

        $this->render('main/index', compact('articles'), 'home');
    }
}