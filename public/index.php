<?php

use App\Autoloader;
use App\Core\Main;

//On définie une constatnte contenant le dossier racine du projet 

define('ROOT', dirname(__DIR__));

//on importe l'autoloader 

require_once ROOT.'/autoloader.php';
Autoloader::register();

//on va instancie Main et Main est notre routeur

$app = new Main();

//on démarre l'application 

$app->start();