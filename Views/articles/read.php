<article>
    <div class="d-flex flex-column align-items-center justify-content-center">
        <div class="card m-2" style="width: 18rem;">
            <img src="/images/news.png" class="card-img-top" alt="image d'un journal">
            <div class="card-body">
                <h2 class="card-title"><?= $article->title ?></h2>
                <h4 class="card-title"><?= $category->name ?></h4>
                <div>
                    <p class="card-text"><?= $article->content ?></p>
                    <small>Auteur : <b><?= $author->nom ?></b></small>
                </div>
                <div>
                    <p>Publié le : <?= $formatDate ?>.</p>
                </div>
                <div class="d-flex justify-content-center">
                    <div><a href="/articles" class="btn btn-warning">Retour</a></div>
                </div>
            </div>
        </div>
    </div>
</article>