<div class="card m-2">
    <div class="card-body">
        <h5 class="card-title text-center">A propos</h5>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio quae exercitationem nostrum aspernatur
        perferendis porro minima, iure perspiciatis totam recusandae soluta nisi possimus accusantium tenetur inventore
        culpa voluptatem labore incidunt.
        Adipisci quisquam, tenetur ea recusandae veniam illo rerum architecto, sequi molestiae quia amet, quaerat nihil
        possimus quibusdam tempore porro dolores? Explicabo soluta corrupti eos architecto quia modi temporibus, ea
        non.
        <em>Pour apprendre la POO en PHP.</em>
    </div>
</div>