<h1 class="text-center">News Plus home</h1>

<div class="container">
    <div class="d-flex flex-column align-items-center justify-content-center">
        <?php foreach($articles as $article): ?>

        <?php $date1 = $article->created_at; ?>

        <div class="card m-2" style="width: 18rem;">
            <img src="/images/news.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?= $article->title ?></h5>
                <p class="card-text">La date de publication : le <?= $article->created_at ?>.</p>
                </p>
                <a href="/articles/read/<?= $article->id ?>" class="btn btn-primary">Plus de détails</a>
            </div>
        </div>


        <?php endforeach; ?>

    </div>
</div>