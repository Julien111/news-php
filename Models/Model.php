<?php

namespace App\Models;

use App\Core\Db;

class Model extends Db 
{
    // la table de la base de données 
    protected $tableName;

    //instance de Db 
    private $db;

    //Le but avoir accès à la liste totale des données dans la table
    public function findAll()
    {
        $query = $this->requete('SELECT * FROM '. $this->table);
        return $query->fetchAll();
    }

    /**
     * fonction findBy 
     * Elle va chercher des éléments en fonction de critères
     * 
     * @param array $criteres
     * @return self
     */
    public function findBy(array $critere)
    {
        $champs = [];
        $valeurs = [];

        //On boucle pour diviser le tableau 
        foreach ($critere as $champ => $valeur){
            // SELECT * annonces WHERE actif = ?
            //on veut faire un bind value

            $champs[] = "$champ = ?";
            $valeurs[] = $valeur;
        }

        //On transforme le tableau champs en string et si on a plusieurs critères, on rajoute des AND pr mysql
        $liste_champs = implode( ' AND ', $champs);

         //on exécute la requête SQL

        return $this->requete('SELECT * FROM '. $this->table.' WHERE '. $liste_champs, $valeurs)->fetchAll();

    }

    /**
     * fonction find ()
     * Le but de cette fonction est de prendre l'id et de return l'élément
     * le fetch récupère une ligne en BDD
     *
     * @param integer $id
     * @return self
     */
    public function find(int $id)
    {
        return $this->requete('SELECT * FROM '. $this->table.' WHERE id = '. $id)->fetch();
    }

    /**
     * fonction pour ajouter un elt dans la base de données
     *
     * @param Model $model
     * @return self
     */
    // on enlève $model car cela va créer $user->create($user) 
    // on met $this
    public function create ()
    {
        //on définit les variables

        $champs = [];
        $inter = [];
        $valeurs = []; 

         //on boucle pour éclater le tableau

        foreach($this as $champ => $valeur){
            // ex avec articles INSERT INTO articles (titre, description, actif) VALUES (?, ?, ?)
            if($valeur !== null && $champ != "db" && $champ != 'table' && $champ != 'tableName'){
                $champs[] = $champ;
                $inter[] = "?";
                $valeurs[] = $valeur;
            }
        }
        
        //On transforme le tableau champs en string et si on a plusieurs critères, on rajoute des virgules pr mysql
        $liste_champs = implode(', ', $champs);
        $liste_inter = implode(', ', $inter);

        //on exécute la requête SQL

        return $this->requete('INSERT INTO '.$this->table.' ('. $liste_champs.') VALUES('.$liste_inter.')', $valeurs);
    }

    //fonction update on a enlevé $model et $id car on reçoit un objet avec toutes les données
    //donc on met $this pour remplacer modèle et $this->id pour remplacer $id

    /**
     * fonction update va permettre de modifier un elt dans la BDD
     *
     * @return self
     */
    public function update()
    {
        //on définit les variables
        $champs = [];
        $valeurs = []; 

        //on boucle pour éclater le tableau
        foreach ($this as $champ => $valeur){
            // Update annonces SET (titre = ?, description = ?, actif = ?) Where id=""
            if($valeur !== null && $champ != "db" && $champ != 'table' && $champ != 'tableName'){
                $champs[] = "$champ = ?";
                $valeurs[] = $valeur;
            }
        }
        $valeurs[] = $this->id;
        //On transforme le tableau champs en string et si on a plusieurs critères, on rajoute des virgules pr mysql
        $liste_champs = implode(',', $champs);

        //on exécute la requête SQL
        return $this->requete('UPDATE '.$this->table.' SET '. $liste_champs.' WHERE id= ?', $valeurs);
    }

    //fonction delete 

    public function delete (int $id)
    {
        return $this->requete("DELETE FROM {$this->table} WHERE id = ?", [$id]);
    }

    //La methode requete
    // la fonction requete fait l'execute et le prepare

    public function requete (string $sql, array $attributs = null)
    {
        //On récupère l'instance de db 
        $this->db = Db::getInstance();

        //On vérifie si on a des attributs

        if($attributs !== null){

            //requête préparée 
            $query = $this->db->prepare($sql);
            $query->execute($attributs);
            return $query;
        }
        else{
            //requête simple 
            return $this->db->query($sql);
        }
    }

    //méthode d'hydratation
    //passer un tableau, à un objet puis 

    /**
     * function hydrate
     * fonction qui permet de rajouter une annonce dans la BDD
     * @param mixed $datas
     * Retourne l'objet hydraté
     */
    //modification de hydrate car on avait dit dans Db qu'on reçoit des objet et en paramètres on a mis array

    public function hydrate ($datas)
    {
        foreach ($datas as $key => $value)
        {
            //on récupère le nom du setter correspondant à la clé (attribut)
            //titre -> setTitre() donc la clé titre devient setTitre avec le T maj
            //avec ucfirst on place la majucule

            $setter = 'set'.ucfirst($key);

            //on vérifie que le setter existe

            if(method_exists($this, $setter)){
                //on appelle le setter 
                $this->$setter($value);
            }
        }
        return $this;
    }
}