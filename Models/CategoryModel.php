<?php

namespace App\Models;

class CategoryModel extends Model
{

    /**
     * id de la category
     *
     * @var int
     */
    protected $id;

    protected $name;

    //ici on déclare le nom de la table articles

    public function __construct()
    {
        $this->table = 'category';
    }

    /**
     * Obtenir l'id de la category
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id de la category
     *
     * @param  int  $id  id de la category
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Obtenir le nom de la catégorie
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
}