<?php

namespace App\Models;

class ArticlesModel extends Model 
{
    /**
     * id de l'annonce
     *
     * @var integer
     */
    protected $id;

    /**
     * titre de l'annonce
     *
     * @var string
     */
    protected $title;

    /**
     * description de l'annonce
     *
     * @var string
     */
    protected $content;

    /**
     * date de publication de l'annonce
     *
     * @var datetime
     */
    protected $created_at;

    /**
     * id de la categorie
     *
     * @var int
     */
    protected $category_id;

    /**
     * id du user
     *
     * @var int
     */
    protected $users_id;

    //ici on déclare le nom de la table articles

    public function __construct()
    {
        $this->table = 'articles';
    }
    

    /**
     * Get id de l'article
     *
     * @return  integer
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id de l'article
     *
     * @param  integer  $id  id de l'articles
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get titre de l'article
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titre de l'article
     *
     * @param  string  $title  titre de l'article
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description de l'article
     *
     * @return  string
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sur le contenu de l'article
     *
     * @param  string  $content  contenu de l'article
     *
     * @return  self
     */ 
    public function setContent(string $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get created_at est la date de publication de l'article
     */ 
    public function getCreated_at()
    {
        return $this->created_at;
    }

    /**
     * Set the value of created_at
     * Va modifier la date de publication
     *
     * @return  self
     */ 
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get id de la categorie
     *
     * @return  int
     */ 
    public function getCategory_id():int
    {
        return $this->category_id;
    }

    /**
     * Set id de la categorie
     *
     * @param  int  $category_id  id de la categorie
     *
     * @return  self
     */ 
    public function setCategory_id(int $category_id)
    {
        $this->category_id = $category_id;

        return $this;
    }

    /**
     * Get id du user
     *
     * @return  int
     */ 
    public function getUsers_id():int
    {
        return $this->users_id;
    }

    /**
     * Set id du user
     *
     * @param  int  $users_id  id du user
     *
     * @return  self
     */ 
    public function setUsers_id(int $users_id)
    {
        $this->users_id = $users_id;

        return $this;
    }

    public function lastArticles ()
    {
        $query = $this->requete('SELECT * FROM '. $this->table . ' ORDER BY created_at DESC LIMIT 5');
        return $query->fetchAll();
    }
}