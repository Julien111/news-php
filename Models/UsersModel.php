<?php

namespace App\Models;

class UsersModel extends Model 
{
    /**
     * id du user
     *
     * @var int
     */
    protected $id;

    protected $nom;

    protected $email;

    protected $password;

    protected $roles;


    public function __construct()
    {
        //ici on veut garder que users qui est le nom de notre table
        $class = str_replace(__NAMESPACE__.'\\', '', __CLASS__);
        $this->table = strtolower(str_replace('Model', '', $class));
    }

    /**
     * Récupérer un email à partir de son email
     *
     * @param string $email
     * @return mixed
     */

    public function findOneByEmail(string $email)
    {
        return $this->requete("SELECT * FROM $this->table WHERE email = ?", [$email])->fetch();
    }

    /**
     * Récupérer un email pour comparer
     *
     * @param string $email
     * @return mixed
     */

    public function findTheEmail(string $email)
    {
        return $this->requete("SELECT email FROM $this->table WHERE email = ?", [$email])->fetch();
    }

    /**
     * Récupérer le nom du user
     *
     * @param integer $id
     * @return string
     */
    public function findAUser(int $id)
    {
        return $this->requete("SELECT nom FROM $this->table WHERE id = ?", [$id])->fetch();
    }

    /**
     * Méthode qui va créer la session utilisateur
     *
     * @return void
     */
    public function setSession()
    {
        $_SESSION['user'] = [
            'id' => $this->id,
            'nom' => $this->nom,
            'email' => $this->email,
            'roles' => $this->roles,
        ];
    }

    /**
     * Get id du user
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id du user
     *
     * @param  int  $id  id du user
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * email du user
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Password du user
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Le nom du user
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of roles
     */ 
    public function getRoles():array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Set the value of roles
     *
     * @return  self
     */ 
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }
}