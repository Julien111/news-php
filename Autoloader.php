<?php 

namespace App;

class Autoloader
{
    //spl_autoload_register enregistre une fonction en tant qu'implémentation de __autoload()

    static function register()
    {
        spl_autoload_register([
            __CLASS__,
            'autoload',
        ]);
    }

    static function autoload($class)
    {
        //On récupère la totalité du namespace de la classe concernée, on va enlever une partie du chemin (App\ pour simplifier)
        //on retire App\ et on a (Client\Compte)
        $class = str_replace(__NAMESPACE__ . '\\', '', $class);

        //On remplace les \ par les /

        $class = str_replace('\\', '/', $class);

        // $fichier stock le chemin d'accès total da la class 
        $fichier = __DIR__ . '/' . $class . '.php';

        //On vérifie que la class existe (le fichier)

        if(file_exists($fichier)){
            require_once $fichier;
        }
    }
}