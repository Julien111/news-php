<?php

namespace App\Core;

use App\Controllers\MainController;

/**
 * Routeur principal
 */
class Main  
{
    public function start()
    {

        //On démarre la session 
        session_start();

        // on doit retirer le trailing slash de l'url 
        $uri = $_SERVER['REQUEST_URI'];

        // $uri récupère les params de l'url, ensuite on va tester si il y a un /

        if(!empty($uri) && $uri != '/' && $uri[-1] === "/"){
            // On enlève le slash  
            $uri = substr($uri, 0, -1);

            //On envoie le code de redirection permanent  

            http_response_code(301);

            //on redirige sans le slash 

            header('Location: '.$uri);
        }

        //On va devoir gérer les params ex = p=controleur/methode/param
        //On sépare les paramètres dans un tableau  
        
        $params = explode('/', $_GET['p']);

        if($params[0] != ''){
            // La première partie c'est le nom du controller à instancier  
            // On met une majuscule en première lettre et on ajoute "controleur" ensuite

            $controller = '\\App\\Controllers\\'.ucfirst(array_shift($params)).'Controller';

            // Le but est dans l'url pour afficher la page et d'utiliser la page
            $controller = new $controller();

            //On doit vérifier si il y a une méthode à la suite en 2éme param

            $action = (isset($params[0])) ? array_shift($params) : 'index';

             //vérification de la méthode dans les controleurs sinon page 404

            if(method_exists($controller, $action)){
                //si il reste des paramètres on les passe à la méthode 
                (isset($params[0])) ? call_user_func_array([$controller, $action], $params) : $controller->$action();
            }
            else{
                http_response_code(404);
                echo "La page recherchée n'existe pas.";
            }

            
        }else{
            //On a pas de paramètre  on instancie le controleur par défaut
            $controller = new MainController;

            //on utilise la méthode index
            $controller->index();
        }


    }
}