<?php

namespace App\Core;

//on importe PDO
// methode du singleton

use PDO;
use PDOException;

class Db extends PDO
{
    //une instance unique de la classe database

    private static $instance;

    //information de connexions

    private const DBHOST = 'localhost';
    private const DBUSER = 'root';
    private const DBPASS = 'root';
    private const DBNAME = 'news';
    private const DBCHARSET = 'utf8';

    //DB_DRIVER . ":host=" . self::DBHOST . ";dbname=" .self::DBNAME;

    private function __construct ()
    {
        //DSN de connexions

        $_dsn = 'mysql:host=' . self::DBHOST . ';dbname=' . self::DBNAME . ';charset=' . self::DBCHARSET;

        //on appelle le constructeur de la classe PDO 

        try{
            parent::__construct($_dsn, self::DBUSER, self::DBPASS); 
            
             //$this ici c'est PDO 
            //FETCH_ASSOC permet d'avoir un tableau associatif pour récupérer les données

            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        }catch(PDOException $e){
            die($e->getMessage());
        } 
    }

    public static function getInstance():self 
    {
        //cette méthode va nous permettre d'appeler la class Db (self) une seule fois
        //Si il y a déjà une instance, on la return

        if(self::$instance === null){
            self::$instance = new self();
        }
        return self::$instance;
    }
}